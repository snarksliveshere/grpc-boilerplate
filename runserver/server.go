package runserver

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	config2 "gitlab.com/snarksliveshere/grpc-boilerplate/config"
)

type Server struct {
	http.Server
}

func (srv *Server) Run() {
	fmt.Println("Server is starting...")

	go func(srv *Server) {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			fmt.Errorf("Server binding error:", err)
		}
	}(srv)

	srv.terminate()
}

func (srv *Server) terminate() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL, syscall.SIGSTOP)

	fmt.Printf("Server started at ", srv.Addr)

	oscall := <-c
	fmt.Printf("Server is terminating with system call: %+v...", oscall)

	cfg := config2.Serv
	baseContext := context.Background()
	ctxShutDown, cancel := context.WithTimeout(baseContext, cfg.Delay)
	defer func() {
		cancel()
	}()

	// timeout handled here because of shutdownPollInterval (500ms) limit in Shutdown method
	<-ctxShutDown.Done()

	if err := srv.Shutdown(baseContext); err != nil {
		log.Fatalf("Server shutdown failed: %+s", err)
	}

	fmt.Printf("Server stopped with delay ", cfg.Delay)
}
