package main

import (
	"context"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/prometheus/common/log"
	"gitlab.com/snarksliveshere/grpc-boilerplate/config"
	"gitlab.com/snarksliveshere/grpc-boilerplate/internal/app/api/v1/admin"
	"gitlab.com/snarksliveshere/grpc-boilerplate/internal/app/api/v1/logger"
	adminv1 "gitlab.com/snarksliveshere/grpc-boilerplate/proto/admin/v1"
	loggerv1 "gitlab.com/snarksliveshere/grpc-boilerplate/proto/logger/v1"
	"gitlab.com/snarksliveshere/grpc-boilerplate/server"
	"google.golang.org/grpc"
)

func main() {
	ctx := context.Background()
	config.Serv = config.Server{
		Log: config.Log{
			Level:  "trace",
			Format: "json",
		},
		Name:           "mtq",
		Port:           9090,
		IsEncoded:      false,
		CookieAge:      0,
		HTTPTimeout:    0,
		Debug:          true,
		Delay:          5,
		MetricsEnabled: false,
		HTTPPort:       9091,
		AdminHTTPPort:  9094,
		GRPCPort:       9092,
	}
	//TestHostGRPC      string `envconfig:"TEST_HOST_GRPC" default:"localhost:8082"`
	//TestHostHTTP      string `envconfig:"TEST_HOST_HTTP" default:"localhost:8088"`
	//TestHostHTTPAdmin string `envconfig:"TEST_HOST_HTTP_ADMIN" default:"localhost:8086"`

	app, err := server.NewServer(config.Serv, append(
		server.GetCorsOpts([]string{}),
	)...,
	)
	if err != nil {
		log.Fatalf("cant init server:%v", err)
	}

	gwmux := runtime.NewServeMux()

	s := grpc.NewServer(
	//grpc.WithUnaryInterceptor()
	)

	loggerv1.RegisterLoggerStoreServiceServer(s, logger.NewLogger(ctx))
	adminv1.RegisterUserServiceServer(s, admin.NewAdmin(ctx))

	err = loggerv1.RegisterLoggerStoreServiceHandlerServer(ctx, gwmux, logger.NewLogger(ctx))
	FatalIfErr(err, "cant init logger")
	err = adminv1.RegisterUserServiceHandlerServer(ctx, gwmux, admin.NewAdmin(ctx))
	FatalIfErr(err, "cant init admin")

	err = app.Run(s, gwmux)
	FatalIfErr(err, "can't run app boilerplate")
}

func FatalIfErr(err error, what string) {
	if err != nil {
		log.Fatalf("%s: %s", what, err)
	}
}
