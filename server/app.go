package server

import (
	"fmt"
	"sync"
	"syscall"

	"github.com/go-chi/chi"
	config2 "gitlab.com/snarksliveshere/grpc-boilerplate/config"
	"gitlab.com/snarksliveshere/grpc-boilerplate/server/app"
	"gitlab.com/snarksliveshere/grpc-boilerplate/server/closer"
	"google.golang.org/grpc"
)

type App struct {
	//info         application.Info
	//publicServer http.Handler
	publicServer chi.Router
	adminServer  chi.Router
	grpcServer   *grpc.Server
	//version      transport.ServiceDesc
	//hc           healthcheck.Handler
	opts        *app.Options
	lis         *listeners
	startTime   string
	startTimeMx sync.RWMutex
	term        int32
	//desc         transport.ServiceDesc

	// closer for public interfaces
	publicCloser *closer.Closer
	// closer for admin interfaces
	adminCloser *closer.Closer
}

func NewServer(c config2.Server, opts ...Option) (*App, error) {
	o, err := useServerOptions(c)
	if err != nil {
		return nil, err
	}
	a := &App{opts: o, publicCloser: closer.New(syscall.SIGTERM, syscall.SIGINT), adminCloser: closer.New()}

	lst, err := newListeners(a.opts)
	if err != nil {
		return nil, fmt.Errorf("can't start listeners, err:%v", err)
	}
	a.lis = lst

	a.initAdminHTTP()
	a.runAdminHTTP()

	return a, nil
}

func useServerOptions(c config2.Server) (*app.Options, error) {
	oo := &app.Options{
		ResolvedOpts: app.ResolvedOpts{
			Hostname:  c.Name,
			PortHTTP:  c.HTTPPort,
			PortAdmin: c.AdminHTTPPort,
			PortGRPC:  c.GRPCPort,
			Debug:     c.Debug,
		},
	}

	return oo, nil
}
