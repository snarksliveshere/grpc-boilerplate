package app

import (
	"crypto/tls"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"google.golang.org/grpc"
)

// ResolvedOpts is a set of knobs controllable by Options.
type ResolvedOpts struct {
	Hostname  string
	PortHTTP  int
	PortAdmin int
	PortGRPC  int

	Debug bool

	AdminServer                   chi.Router
	UnaryInterceptor              []grpc.UnaryServerInterceptor
	StreamInterceptor             []grpc.StreamServerInterceptor
	ServiceDescriptionInterceptor []grpc.UnaryServerInterceptor
	GrpcServerOptions             []grpc.ServerOption

	CorsOptions cors.Options

	PublicTLSConfig *tls.Config

	PublicMiddleware []func(http.Handler) http.Handler
	AdminMiddleware  []func(http.Handler) http.Handler
}

// Options holds configuration for an `App`.
type Options struct {
	ResolvedOpts
	DisableErrorLogInterceptor bool
}
