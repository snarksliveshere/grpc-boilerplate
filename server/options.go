package server

import (
	"net/http"

	"gitlab.com/snarksliveshere/grpc-boilerplate/server/app"
)

type Option interface {
	Apply(*app.Options) error
}

type optionFn func(*app.Options) error

func (fn optionFn) Apply(opts *app.Options) error {
	return fn(opts)
}

func GetCorsOpts([]string) []Option {
	return GetCorsOptsWithCustomOrigins([]string{})
}

func GetCorsOptsWithCustomOrigins(origins []string) []Option {
	if len(origins) == 0 {
		return nil
	}
	baseOrigins := []string(nil)

	return []Option{
		WithCORSAllowedAuthentication(true),
		WithCORSAllowedOrigins(append(baseOrigins, origins...)),
		WithCORSAllowedMethods([]string{"GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS"}),
		WithCORSAllowedHeaders([]string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "X-Platform"}),
		WithCORSMaxAge(3600),
		WithCORSExposeHeaders([]string{"Link"}),
	}
}

func WithCORSExposeHeaders(exposeHeaders []string) Option {
	return optionFn(func(opts *app.Options) error {
		opts.CorsOptions.ExposedHeaders = exposeHeaders
		return nil
	})
}

func WithCORSMaxAge(age int) Option {
	return optionFn(func(opts *app.Options) error {
		opts.CorsOptions.MaxAge = age
		return nil
	})
}

func WithCORSAllowedHeaders(headers []string) Option {
	return optionFn(func(opts *app.Options) error {
		opts.CorsOptions.AllowedHeaders = headers
		return nil
	})
}

func WithCORSAllowedOrigins(origins []string) Option {
	return optionFn(func(opts *app.Options) error {
		opts.CorsOptions.AllowedOrigins = origins
		return nil
	})
}

func WithCORSAllowedAuthentication(allow bool) Option {
	return optionFn(func(opts *app.Options) error {
		opts.CorsOptions.AllowCredentials = allow
		return nil
	})
}

func WithCORSAllowedMethods(methods []string) Option {
	return optionFn(func(opts *app.Options) error {
		opts.CorsOptions.AllowedMethods = methods
		return nil
	})
}

// WithPublicMiddleware adds http.Handler middleware for public HTTP server
func WithPublicMiddleware(mw ...func(http.Handler) http.Handler) Option {
	return optionFn(func(opts *app.Options) error {
		opts.PublicMiddleware = append(opts.PublicMiddleware, mw...)
		return nil
	})
}

// WithAdminMiddleware adds http.Handler middleware for admin HTTP server
func WithAdminMiddleware(mw ...func(http.Handler) http.Handler) Option {
	return optionFn(func(opts *app.Options) error {
		opts.AdminMiddleware = append(opts.AdminMiddleware, mw...)
		return nil
	})
}
