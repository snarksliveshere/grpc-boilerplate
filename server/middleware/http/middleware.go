package http

import (
	"compress/gzip"
	"net/http"

	chimw "github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
)

func Middleware(opts ...Option) []func(http.Handler) http.Handler {
	o := evaluateOptions(opts)

	c := cors.New(o.corsOptions)

	mw := []func(http.Handler) http.Handler{
		mwGzipRequest,
		c.Handler,
		chimw.RequestID,
		chimw.StripSlashes,
		//Recover,
	}

	mw = append(mw, o.customMiddleware...)

	return mw
}

func mwGzipRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Header.Get("Content-Encoding") {
		case "gzip":
			var err error
			r.Body, err = gzip.NewReader(r.Body)
			if err != nil {
				w.WriteHeader(400)
				return
			}
		default:
		}
		next.ServeHTTP(w, r)
	})
}
