package http

import (
	"net/http"

	"github.com/go-chi/cors"
)

type options struct {
	corsOptions      cors.Options
	customMiddleware []func(http.Handler) http.Handler
	opNameFunc       operationNameFunc
}

type operationNameFunc func(*http.Request) string

func evaluateOptions(opts []Option) *options {
	o := &options{}
	for _, op := range opts {
		op(o)
	}
	return o
}

// Option controls the behavior of the Middleware chaining.
type Option func(*options)

// WithCORSOptions set CORS
func WithCORSOptions(corsOpts cors.Options) Option {
	return func(opts *options) {
		opts.corsOptions = corsOpts
	}
}

// WithCustomMiddleware set other middlewares
func WithCustomMiddleware(mw ...func(http.Handler) http.Handler) Option {
	return func(opts *options) {
		opts.customMiddleware = mw
	}
}
