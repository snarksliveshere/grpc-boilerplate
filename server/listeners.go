package server

import (
	"crypto/tls"
	"fmt"
	"net"

	"github.com/pkg/errors"

	"gitlab.com/snarksliveshere/grpc-boilerplate/server/app"
)

type listeners struct {
	http      net.Listener
	httpAdmin net.Listener
	grpc      net.Listener
}

func newListeners(opt *app.Options) (*listeners, error) {
	l := &listeners{}

	dbg, err := net.Listen("tcp", fmt.Sprintf(":%v", opt.PortAdmin))
	if err != nil {
		return nil, errors.Wrap(err, "couldn't create AdminPort listener")
	}
	l.httpAdmin = dbg

	if opt.PublicTLSConfig == nil {
		http, err := net.Listen("tcp", fmt.Sprintf(":%v", opt.PortHTTP))
		if err != nil {
			return nil, errors.Wrap(err, "couldn't create HTTP listener")
		}
		l.http = http
	} else {
		http, err := tls.Listen("tcp", fmt.Sprintf(":%v", opt.PortHTTP), opt.PublicTLSConfig)
		if err != nil {
			return nil, errors.Wrap(err, "couldn't create TLS listener")
		}
		l.http = http
	}

	grpc, err := net.Listen("tcp", fmt.Sprintf(":%v", opt.PortGRPC))
	if err != nil {
		return nil, errors.Wrap(err, "couldn't create RPC listener")
	}
	l.grpc = grpc

	return l, nil
}
