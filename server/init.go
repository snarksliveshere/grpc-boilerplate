package server

import (
	"net/http/pprof"

	"github.com/go-chi/chi"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/snarksliveshere/grpc-boilerplate/server/middleware/http"
)

func (a *App) initPublicHTTP(gwMux *runtime.ServeMux) {
	a.publicServer = chi.NewRouter()

	a.publicServer.Use(
		http.Middleware(
			http.WithCORSOptions(a.opts.CorsOptions),
			http.WithCustomMiddleware(a.opts.PublicMiddleware...),
		)...,
	)

	a.publicServer.MethodNotAllowed(gwMux.ServeHTTP)
	a.publicServer.NotFound(gwMux.ServeHTTP)
}

func (a *App) initAdminHTTP() {
	if a.opts.AdminServer != nil {
		a.adminServer = a.opts.AdminServer
	} else {
		a.adminServer = chi.NewMux()
	}

	if a.opts.AdminMiddleware != nil {
		a.adminServer.Use(a.opts.AdminMiddleware...)
	}
	a.adminServer.Handle("/metrics", promhttp.Handler())

	if a.opts.Debug {
		a.adminServer.HandleFunc("/debug/pprof", pprof.Index)
		a.adminServer.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		a.adminServer.HandleFunc("/debug/pprof/profile", pprof.Profile)
		a.adminServer.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		a.adminServer.HandleFunc("/debug/pprof/trace", pprof.Trace)
		a.adminServer.Handle("/debug/pprof/block", pprof.Handler("block"))
		a.adminServer.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
		a.adminServer.Handle("/debug/pprof/heap", pprof.Handler("heap"))
		a.adminServer.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	}
}
