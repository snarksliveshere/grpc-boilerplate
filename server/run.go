package server

import (
	"context"
	"net/http"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/prometheus/common/log"
	"gitlab.com/snarksliveshere/grpc-boilerplate/server/closer"
	"google.golang.org/grpc"

	"github.com/pkg/errors"
)

var (
	gracefulDelay   = 1 * time.Second
	gracefulTimeout = 3 * time.Second
)

// runAdminHTTP is called in New function
func (a *App) runAdminHTTP() {
	adminServer := &http.Server{Handler: a.adminServer}
	go func() {
		if err := errors.Wrap(adminServer.Serve(a.lis.httpAdmin), "http.admin"); err != nil && errors.Cause(err) != http.ErrServerClosed {
			log.Error(context.Background(), err.Error())
			a.adminCloser.CloseAll()
		}
	}()
	a.adminCloser.Add(func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		adminServer.SetKeepAlivesEnabled(false)
		if err := errors.Wrap(adminServer.Shutdown(ctx), "http.admin: error during shutdown"); err != nil {
			return err
		}
		log.Warn(context.Background(), "http.admin: gracefully stopped")
		return nil
	})
}

func (a *App) Run(s *grpc.Server, gwMux *runtime.ServeMux) error {
	a.grpcServer = s
	a.runGRPC()

	//mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{OrigName: true, EmitDefaults: true}))
	//runtime.SetHTTPBodyMarshaler(mux)
	//opts := []grpc.DialOption{
	//	grpc.WithInsecure(),
	//	grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(50000000)),
	//}

	a.initPublicHTTP(gwMux)

	a.runPublicHTTP()

	a.startTimeMx.Lock()
	a.startTime = time.Now().Format(time.RFC3339)
	a.startTimeMx.Unlock()

	// Wait signal and close all Scratch resources
	a.publicCloser.Wait()
	// Close all other resources from globalCloser
	closer.CloseAll()
	// Close all Scratch admin resources
	a.adminCloser.CloseAll()

	return nil
}

func (a *App) runGRPC() {
	if a.grpcServer != nil {
		go func() {
			if err := errors.Wrap(a.grpcServer.Serve(a.lis.grpc), "grpc"); err != nil {
				log.Error(context.Background(), err.Error())
				a.publicCloser.CloseAll()
			}
		}()
		a.publicCloser.Add(func() error {
			ctx, cancel := context.WithTimeout(context.Background(), gracefulTimeout)
			defer cancel()

			log.Warn(context.Background(), "grpc: waiting stop of traffic")
			time.Sleep(gracefulDelay)
			log.Warn(context.Background(), "grpc: shutting down")

			done := make(chan struct{})
			go func() {
				a.grpcServer.GracefulStop()
				close(done)
			}()
			select {
			case <-done:
				log.Warn(context.Background(), "grpc: gracefully stopped")
			case <-ctx.Done():
				err := errors.Wrap(ctx.Err(), "grpc: error during shutdown server")
				a.grpcServer.Stop()
				return errors.Wrap(err, "grpc: force stopped")
			}
			return nil
		})
	}
}

func (a *App) runPublicHTTP() {
	publicServer := &http.Server{Handler: a.publicServer}
	go func() {
		if err := errors.Wrap(publicServer.Serve(a.lis.http), "http.public"); err != nil && errors.Cause(err) != http.ErrServerClosed {
			log.Error(context.Background(), err.Error())
			a.publicCloser.CloseAll()
		}
	}()
	a.publicCloser.Add(func() error {
		ctx, cancel := context.WithTimeout(context.Background(), gracefulTimeout)
		defer cancel()

		log.Warn(context.Background(), "http.public: waiting stop of traffic")
		time.Sleep(gracefulDelay)
		log.Warn(context.Background(), "http.public: shutting down")

		publicServer.SetKeepAlivesEnabled(false)
		if err := errors.Wrap(publicServer.Shutdown(ctx), "http.public: error during shutdown"); err != nil {
			return err
		}
		log.Warn(context.Background(), "http.public: gracefully stopped")
		return nil
	})
}
