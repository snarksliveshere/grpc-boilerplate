generate:
	${GOPATH}/bin/buf generate

lint:
	${GOPATH}/bin/buf lint

BUF_VERSION:=0.55.0

install:
	curl -sSL \
    	"https://github.com/bufbuild/buf/releases/download/v${BUF_VERSION}/buf-$(shell uname -s)-$(shell uname -m)" \
    	-o "$(shell go env GOPATH)/bin/buf" && \
  	chmod +x "$(shell go env GOPATH)/bin/buf"
