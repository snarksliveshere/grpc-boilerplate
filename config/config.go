package config

import (
	"time"
)

var Serv Server

type Server struct {
	Log            Log
	Name           string        `required:"true"`
	Port           int           `required:"true"`
	IsEncoded      bool          `required:"true" split_words:"true"`
	CookieAge      time.Duration `default:"720h" split_words:"true"`
	HTTPTimeout    time.Duration `default:"5s" split_words:"true"`
	Debug          bool
	Delay          time.Duration `default:"3s"`
	MetricsEnabled bool          `default:"true" split_words:"true"`

	HTTPPort      int `required:"true"`
	AdminHTTPPort int `required:"true"`
	GRPCPort      int `required:"true"`
}

type Log struct {
	Level  string `required:"true"`
	Format string `default:"text"`
}
