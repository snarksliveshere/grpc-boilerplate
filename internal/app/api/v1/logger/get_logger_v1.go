package logger

import (
	"context"

	loggerv1 "gitlab.com/snarksliveshere/grpc-boilerplate/proto/logger/v1"
)

func (i Implementation) GetLogger(ctx context.Context, in *loggerv1.GetLoggerRequest) (*loggerv1.GetLoggerResponse, error) {
	panic("implement me")
}
