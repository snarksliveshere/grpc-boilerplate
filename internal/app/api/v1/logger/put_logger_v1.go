package logger

import (
	"context"

	loggerv1 "gitlab.com/snarksliveshere/grpc-boilerplate/proto/logger/v1"
)

func (i Implementation) PutLogger(ctx context.Context, in *loggerv1.PutLoggerRequest) (*loggerv1.PutLoggerResponse, error) {
	panic("implement me")
}
