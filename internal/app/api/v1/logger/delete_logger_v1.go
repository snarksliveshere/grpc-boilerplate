package logger

import (
	"context"

	loggerv1 "gitlab.com/snarksliveshere/grpc-boilerplate/proto/logger/v1"
)

func (i Implementation) DeleteLogger(ctx context.Context, in *loggerv1.DeleteLoggerRequest) (*loggerv1.DeleteLoggerResponse, error) {
	panic("implement me")
}
