package logger

import (
	"context"
)

type Implementation struct {
	ctx context.Context
}

func NewLogger(ctx context.Context) *Implementation {
	return &Implementation{
		ctx: ctx,
	}
}
