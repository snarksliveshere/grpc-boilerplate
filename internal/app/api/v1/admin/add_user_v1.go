package admin

import (
	"context"
	"fmt"

	"github.com/go-chi/chi/middleware"
	v1 "gitlab.com/snarksliveshere/grpc-boilerplate/proto/admin/v1"
)

func (i Implementation) AddUser(ctx context.Context, in *v1.AddUserRequest) (*v1.AddUserResponse, error) {
	reqID := middleware.GetReqID(ctx)
	var msg string
	if reqID == "" {
		msg = "grpc_req_olala"
	} else {
		msg = fmt.Sprintf("http_req, reqID:%s", reqID)
	}
	resp := &v1.AddUserResponse{
		Id: msg,
	}
	return resp, nil
}
