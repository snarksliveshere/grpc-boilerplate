package admin

import (
	"context"
)

type Implementation struct {
	ctx context.Context
}

func NewAdmin(ctx context.Context) *Implementation {
	return &Implementation{
		ctx: ctx,
	}
}
