package admin

import (
	"context"

	v1 "gitlab.com/snarksliveshere/grpc-boilerplate/proto/admin/v1"
)

func (i Implementation) ListUsers(ctx context.Context, request *v1.ListUsersRequest) (*v1.ListUsersResponse, error) {
	panic("implement me")
}
